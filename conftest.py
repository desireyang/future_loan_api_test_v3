# -*- coding: utf-8 -*-
# @Time    : 2020/4/20 9:11
# @Author  : Desire
# @Email   : yangyin1106@163.com
# @Blog    : https://www.cnblogs.com/desireyang/
# @File    : conftest.py
# @Software: PyCharm
# Explain  : pytest 存储测试夹具 fixture 的专用模块
import pytest

from common.DB_Handle import MySqlHandle
from common.Logging_Handle import log
from common.ParamRe_Handle import ReplaceData


@pytest.fixture(scope="class", autouse=True)
def set_class():
    log.info("*" * 15 + "接口测试自动化启动" + "*" * 15)
    yield
    log.info("*" * 15 + "接口测试自动化结束" + "*" * 15)


@pytest.fixture(scope="class")
def invest_set_up():
    # 查询数据库获取最新的投资项目id，和剩余可投金额
    mh = MySqlHandle()
    sql = 'SELECT MAX(id),amount FROM futureloan.loan WHERE `status`=2 AND amount>400;'
    loan_id, amount = mh.find_one(sql)

    yield loan_id, amount
