# -*- coding: utf-8 -*-
# @Time    : 2020/4/13 10:11
# @Author  : Desire
# @Email   : yangyin1106@163.com
# @Blog    : https://www.cnblogs.com/desireyang/
# @File    : Path_Handle.py
# @Software: PyCharm

import os

BASE = os.path.abspath(os.path.dirname(__file__))
# 项目路径
BASE_DIR = os.path.dirname(BASE)
# 配置文件路径
CONF_DIR = os.path.join(BASE_DIR, 'conf')
# 测试数据文件路径
DATA_DIR = os.path.join(BASE_DIR, 'data')
# 日志文件路径
LOGS_DIR = os.path.join(BASE_DIR, 'logs')
# 测试报告文件路径
REPORTS_DIR = os.path.join(BASE_DIR, 'reports')

# Allure测试报告存放路径
ALLURE_DIR = os.path.join(REPORTS_DIR, "allure")

# AllurePlus(在allure测试报告基础上升级的测试报告，查看更加简单)测试报告存放路径
ALLURE_PLUS_DIR = os.path.join(REPORTS_DIR, "allure-plus")
