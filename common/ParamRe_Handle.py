# -*- coding: utf-8 -*-
# @Time    : 2020/4/14 15:37
# @Author  : Desire
# @Email   : yangyin1106@163.com
# @Blog    : https://www.cnblogs.com/desireyang/
# @File    : ParamRe_Handle.py
# @Software: PyCharm

"""
正则匹配参数，参数化数据
"""
import re

from common.Yaml_Handle import yh


class ReplaceData(object):
    """保存一些要替换的数据"""
    pass


def replace_data(case_data: str):
    """数据参数格式化"""
    # 匹配规则
    r = "#(.+?)#"
    # 判断匹配结果是否为None
    while re.search(r, case_data):
        # 匹配出第一个要替换的数据
        res = re.search(r, case_data)
        # 提取待替换的内容
        item = res.group()
        # 获取替换内容中的数据项
        key = res.group(1)
        try:
            case_data = case_data.replace(item, yh.get_data("user")[key])
        except KeyError:
            case_data = case_data.replace(item, str(getattr(ReplaceData, key)))
    return case_data
