# -*- coding: utf-8 -*-
# @Time    : 2020/4/16 11:23
# @Author  : Desire
# @Email   : yangyin1106@163.com
# @Blog    : https://www.cnblogs.com/desireyang/
# @File    : test_05_update.py
# @Software: PyCharm
import os
import pytest

import jsonpath

from common.DB_Handle import MySqlHandle
from common.Func_Handle import RandomName
from common.Logging_Handle import log
from common.ParamRe_Handle import ReplaceData, replace_data
from common.Path_Handle import DATA_DIR
from common.Request_Handle import send_requests
from common.Yaml_Handle import yh, YamlHandle

update_yh = YamlHandle(os.path.join(DATA_DIR, 'case_data_05_update.yaml'))
req_yh = yh.get_data("request")
# 请求头
headers = req_yh["headers"]
#
mh = MySqlHandle()
# 随机生成名称
rn = RandomName()


def check_sql(sql):
    """传入从配置文件拿到的sql,然后处理参数化，然后读取余额"""
    return mh.find_one(replace_data(sql))[0]


class TestUpdateCases:
    """更改昵称接口测试"""

    @pytest.mark.parametrize("case", update_yh.get_data("update"))
    def test_update(self, case):
        case_id = case["case_id"]
        title = case["title"]
        method = case["method"]
        setattr(ReplaceData, 'reg_name', rn.create_name())
        # 先把参数转成字符串，调用数据参数化函数，然后通过eval去掉引号
        case_data = eval(replace_data(str(case["data"])))
        expect = case["expect"]
        url = req_yh["url_path"] + case["url"]
        report = send_requests(url=url, method=method, json=case_data, headers=headers)
        result = report.json()
        if 'login' in url:
            # 当url地址中出现login的时候执行

            # 通过jsonpath获取用户id
            member_id = jsonpath.jsonpath(result, "$..id")[0]
            # 把得到的id放到临时保存参数类中，供下面的用例使用
            setattr(ReplaceData, 'member_id', member_id)
            # 获取token
            token_data = jsonpath.jsonpath(result, "$..token_type")[0] + " " + jsonpath.jsonpath(result, "$..token")[0]
            # 把获取到的token添加到headers请求头中
            headers["Authorization"] = token_data
        else:
            try:
                assert expect["code"] == result["code"]
                assert expect["msg"] == result["msg"]
                if result["msg"] == 'OK':
                    reg_name1 = case_data["reg_name"]
                    reg_name2 = check_sql(case["sql"])
                    assert reg_name1 == reg_name2
            except AssertionError as a:
                log.error("==>>{}，预期结果：{}，实际结果：{}，用例执行失败！！！".format(case, expect, result))
                raise a
            else:
                log.info("-->>case_id：{}，title：{}，用例执行成功！".format(case_id, title))
