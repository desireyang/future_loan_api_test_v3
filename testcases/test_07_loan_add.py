# -*- coding: utf-8 -*-
# @Time    : 2020/4/17 8:55
# @Author  : Desire
# @Email   : yangyin1106@163.com
# @Blog    : https://www.cnblogs.com/desireyang/
# @File    : test_07_loan_add.py
# @Software: PyCharm
import os
import pytest

import jsonpath

from common.Logging_Handle import log
from common.Path_Handle import DATA_DIR
from common.Request_Handle import send_requests
from common.Yaml_Handle import YamlHandle, yh
from common.ParamRe_Handle import ReplaceData, replace_data

loan_add_yh = YamlHandle(os.path.join(DATA_DIR, 'case_data_07_loan_add.yaml'))
req_yh = yh.get_data('request')
headers = req_yh["headers"]


class TestLoanAddCases:
    """新增项目接口测试"""

    @pytest.mark.parametrize("case", loan_add_yh.get_data('loan_add'))
    def test_loan_add(self, case):
        case_id = case["case_id"]
        title = case["title"]
        method = case["method"]
        # 先把参数转成字符串，调用数据参数化函数，然后通过eval去掉引号
        case_data = eval(replace_data(str(case["data"])))
        expect = case["expect"]
        url = req_yh["url_path"] + case["url"]
        report = send_requests(url=url, method=method, json=case_data, headers=headers)
        result = report.json()
        if 'login' in url:
            # 当url地址中出现login的时候执行

            # 通过jsonpath获取用户id
            member_id = jsonpath.jsonpath(result, "$..id")[0]
            # 把得到的id放到临时保存参数类中，供下面的用例使用
            setattr(ReplaceData, 'member_id', member_id)
            # 获取token
            token_data = jsonpath.jsonpath(result, "$..token_type")[0] + " " + jsonpath.jsonpath(result, "$..token")[0]
            # 把获取到的token添加到headers请求头中
            headers["Authorization"] = token_data
        else:
            try:
                assert expect["code"] == result["code"]
                assert expect["msg"] == result["msg"]
            except AssertionError as a:
                log.error("==>>{}，预期结果：{}，实际结果：{}，用例执行失败！！！".format(case, expect, result))
                raise a
            else:
                log.info("-->>case_id：{}，title：{}，用例执行成功！".format(case_id, title))
