# -*- coding: utf-8 -*-
# @Time    : 2020/4/13 10:23
# @Author  : Desire
# @Email   : yangyin1106@163.com
# @Blog    : https://www.cnblogs.com/desireyang/
# @File    : test_02_login.py
# @Software: PyCharm
import os
import pytest

from common.Logging_Handle import log
from common.Path_Handle import DATA_DIR
from common.Request_Handle import send_requests
from common.Yaml_Handle import YamlHandle, yh

login_yh = YamlHandle(os.path.join(DATA_DIR, 'case_data_02_login.yaml'))
req_yh = yh.get_data("request")


class TestLoginCases:
    """登录接口测试"""

    @pytest.mark.parametrize("case", login_yh.get_data('login'))
    def test_login(self, case):
        case_id = case["case_id"]
        title = case["title"]
        method = case["method"]
        case_data = case["data"]
        expect = case["expect"]
        url = req_yh["url_path"] + case["url"]
        headers = req_yh["headers"]
        report = send_requests(url=url, method=method, json=case_data, headers=headers)
        result = report.json()
        try:
            assert expect["code"] == result["code"]
            assert expect["msg"] == result["msg"]
        except AssertionError as a:
            log.error("==>>case_id：{}，title：{}，预期结果：{}，实际结果：{}，用例执行失败！！！".format(case_id, title, expect, result))
            raise a
        else:
            log.info("-->>case_id：{}，title：{}，用例执行成功！".format(case_id, title))
