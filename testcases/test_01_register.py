# -*- coding: utf-8 -*-
# @Time    : 2020/4/14 15:32
# @Author  : Desire
# @Email   : yangyin1106@163.com
# @Blog    : https://www.cnblogs.com/desireyang/
# @File    : test_01_register.py
# @Software: PyCharm

import os

import pytest

from common.Func_Handle import random_phone
from common.Logging_Handle import log
from common.ParamRe_Handle import replace_data, ReplaceData
from common.Path_Handle import DATA_DIR
from common.Request_Handle import send_requests
from common.Yaml_Handle import YamlHandle, yh

register_yh = YamlHandle(os.path.join(DATA_DIR, 'case_data_01_register.yaml'))
req_yh = yh.get_data("request")


class TestRegisterCases:
    """注册接口测试"""

    @pytest.mark.parametrize("case", register_yh.get_data("register"))
    def test_register(self, case):
        case_id = case["case_id"]
        title = case["title"]
        method = case["method"]
        # 保存随机手机号到ReplaceData类中
        setattr(ReplaceData, 'mobile_phone', random_phone())
        # 先把参数转成字符串，调用数据参数化函数，然后通过eval去掉引号
        case_data = eval(replace_data(str(case["data"])))
        expect = case["expect"]
        url = req_yh["url_path"] + case["url"]
        headers = req_yh["headers"]
        report = send_requests(url=url, method=method, json=case_data, headers=headers)
        result = report.json()
        try:
            assert expect["code"] == result["code"]
            assert expect["msg"] == result["msg"]
        except AssertionError as a:
            log.error("==>>case_id：{}，title：{}，预期结果：{}，实际结果：{}，用例执行失败！！！".format(case_id, title, expect, result))
            raise a
        else:
            log.info("-->>case_id：{}，title：{}，用例执行成功！".format(case_id, title))
