# -*- coding: utf-8 -*-
# @Time    : 2020/4/16 8:47
# @Author  : Desire
# @Email   : yangyin1106@163.com
# @Blog    : https://www.cnblogs.com/desireyang/
# @File    : test_04_withdraw.py
# @Software: PyCharm
import os
import pytest

import jsonpath

from common.DB_Handle import MySqlHandle
from common.Logging_Handle import log
from common.ParamRe_Handle import ReplaceData, replace_data
from common.Path_Handle import DATA_DIR
from common.Request_Handle import send_requests
from common.Yaml_Handle import yh, YamlHandle

withdraw_yh = YamlHandle(os.path.join(DATA_DIR, 'case_data_04_withdraw.yaml'))
req_yh = yh.get_data("request")
# 请求头
headers = req_yh["headers"]
#
mh = MySqlHandle()


def check_sql(sql):
    """传入从配置文件拿到的sql,然后处理参数化，然后读取余额"""
    return float(mh.find_one(replace_data(sql))[0])


class TestWithdrawCases:
    """提现接口测试"""

    @pytest.mark.parametrize("case", withdraw_yh.get_data("withdraw"))
    def test_withdraw(self, case):
        case_id = case["case_id"]
        title = case["title"]
        method = case["method"]
        # 先把参数转成字符串，调用数据参数化函数，然后通过eval去掉引号
        case_data = eval(replace_data(str(case["data"])))
        expect = case["expect"]
        url = req_yh["url_path"] + case["url"]
        if case["sql"] != '':
            self.leave_amount1 = check_sql(case["sql"])
        report = send_requests(url=url, method=method, json=case_data, headers=headers)
        result = report.json()
        if 'login' in url:
            # 当url地址中出现login的时候执行

            # 通过jsonpath获取用户id
            member_id = jsonpath.jsonpath(result, "$..id")[0]
            # 把得到的id放到临时保存参数类中，供下面的用例使用
            setattr(ReplaceData, 'member_id', member_id)
            # 获取token
            token_data = jsonpath.jsonpath(result, "$..token_type")[0] + " " + jsonpath.jsonpath(result, "$..token")[0]
            # 把获取到的token添加到headers请求头中
            headers["Authorization"] = token_data
        else:

            try:
                assert expect["code"] == result["code"]
                assert expect["msg"] == result["msg"]
                if result["msg"] == 'OK':
                    leave_amount2 = check_sql(case["sql"]) + case_data["amount"]
                    assert self.leave_amount1 == round(leave_amount2, 2)
            except AssertionError as a:
                log.error("==>>{}，预期结果：{}，实际结果：{}，用例执行失败！！！".format(case, expect, result))
                raise a
            else:
                log.info("-->>case_id：{}，title：{}，用例执行成功！".format(case_id, title))
