# -*- coding: utf-8 -*-
# @Time    : 2020/4/17 11:28
# @Author  : Desire
# @Email   : yangyin1106@163.com
# @Blog    : https://www.cnblogs.com/desireyang/
# @File    : test_10_loads.py
# @Software: PyCharm

import os
import pytest

from common.Logging_Handle import log
from common.Path_Handle import DATA_DIR
from common.Request_Handle import send_requests
from common.Yaml_Handle import YamlHandle, yh

loans_yh = YamlHandle(os.path.join(DATA_DIR, 'case_data_10_loans.yaml'))
req_yh = yh.get_data('request')
headers = req_yh["headers"]


class TestLoadsCases:
    """项目列表接口测试"""

    @pytest.mark.parametrize("case", loans_yh.get_data('loans'))
    def test_loads(self, case):
        case_id = case["case_id"]
        title = case["title"]
        method = case["method"]
        case_data = case["data"]
        expect = case["expect"]
        url = req_yh["url_path"] + case["url"]
        report = send_requests(url=url, method=method, params=case_data, headers=headers)
        result = report.json()
        try:
            assert expect["code"] == result["code"]
            assert expect["msg"] == result["msg"]
        except AssertionError as a:
            log.error("==>>{}，预期结果：{}，实际结果：{}，用例执行失败！！！".format(case, expect, result))
            raise a
        else:
            log.info("-->>case_id：{}，title：{}，用例执行成功！".format(case_id, title))
